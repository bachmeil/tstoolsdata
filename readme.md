# Description

This R package holds data that can be used to work examples using the
tstools package. For research purposes, it's best to download your data
yourself. The intended use case for this package is students taking my
econometrics classes.
